//============================================================================
// <Description>     :<Main file for wcstools java wrapper>
// <File>            :<main.cpp>
// <Type>            :<c code>
// <Author>          :<Rafael Morales rmorales@iaa.es>
// <Creation date>   :<04 August 2017>
// <History>         :<None>
//============================================================================

//============================================================================
//Include section
//============================================================================

//============================================================================
//System include

//============================================================================
//User include

#include "gSpark_wrapper_Wcstools__.h"

//============================================================================
//System include

//============================================================================
//Declaration section
//============================================================================


//============================================================================
//External functions
//============================================================================

int init(char * fn);

void close(void);

int xyToSky(double * input,
	double * output,
	unsigned long inputItemSize);

int skyToXY(double * input,
	double * output,
	unsigned long inputItemSize);


//============================================================================
//Code section
//============================================================================

//============================================================================

JNIEXPORT jint JNICALL Java_gSpark_wrapper_Wcstools_00024_init
  (JNIEnv * env, jobject obj, jstring s){
      
	char * fileName = (*env)->GetStringUTFChars( env, s , NULL );
	int r = init(fileName);
    (*env)->ReleaseStringUTFChars(env, s, fileName);
    return r;
}

//============================================================================

JNIEXPORT void JNICALL Java_gSpark_wrapper_Wcstools_00024_close
  (JNIEnv * env, jobject obj){

	close();
}

//============================================================================

JNIEXPORT jint JNICALL Java_gSpark_wrapper_Wcstools_00024_xyToSky
  (JNIEnv * env, jobject obj, jdoubleArray input, jdoubleArray output, jlong inputItemSize){

	double * inputLocal = (*env)->GetDoubleArrayElements(env, input, 0);
    if (inputLocal == NULL) {
    	 printf("\n Error in Java_gSpark_wrapper_Wcstools_00024_xyToSky. Input array.");
	}

	double * outputLocal = (*env)->GetDoubleArrayElements(env, output, 0);
    if (outputLocal == NULL) {
    	 printf("\n Error in Java_gSpark_wrapper_Wcstools_00024_xyToSky. Output array.");
	}

    int r = xyToSky(inputLocal, outputLocal, inputItemSize);
   	if (!r ) printf("\n Error in Java_gSpark_wrapper_Wcstools_00024_xyToSky. Error in xyToSky.");

   	(*env)->ReleaseDoubleArrayElements(env, output, outputLocal, 0);
    (*env)->ReleaseDoubleArrayElements(env, input, inputLocal, 0);

   	return r;
}

//============================================================================
JNIEXPORT jint JNICALL Java_gSpark_wrapper_Wcstools_00024_skyToXY
  (JNIEnv * env, jobject obj, jdoubleArray input, jdoubleArray output, jlong inputItemSize) {

    double * inputLocal = (*env)->GetDoubleArrayElements(env, input, 0);
	if (inputLocal == NULL) {
	   	 printf("\n Error in Java_gSpark_wrapper_Wcstools_00024_skyToXY. Input array.");
	}

	double * outputLocal = (*env)->GetDoubleArrayElements(env, output, 0);
	if (outputLocal == NULL) {
	  	 printf("\n Error in Java_gSpark_wrapper_Wcstools_00024_skyToXY. Output array.");
	}

    int r = skyToXY(inputLocal, outputLocal, inputItemSize);
   	if (!r ) printf("\n Error in Java_gSpark_wrapper_Wcstools_00024_skyToXY. Error in skyToXY.");

   	(*env)->ReleaseDoubleArrayElements(env, output, outputLocal, 0);
    (*env)->ReleaseDoubleArrayElements(env, input, inputLocal, 0);

   	return r;
}

//============================================================================
//============================================================================

//============================================================================
//End of file:main.c
//============================================================================
