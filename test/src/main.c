//Java Wrapper for wcstools.
//http://tdc-www.harvard.edu/wcstools/
//Adapted from wcstools. version 3.9.5
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//Global vars
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//External functions
//-----------------------------------------------------------------------------
extern int init(char * fn);

extern void close(void);

extern int xyToSky(double * input,
	double * output,
	unsigned long inputItemSize);


extern int skyToXY(double * input,
	double * output,
	unsigned long inputItemSize);

//-----------------------------------------------------------------------------
//External functions
//-----------------------------------------------------------------------------

int main(void){

	char * fileName = "/home/router/Downloads/center/105955-323229-ash11-S001_astroimageJ.fits";
	//char * fileName = "/media/data2TB/astrometry/center/105955-323229-ash11-S001_astroimageJ.fits";

	#define MAX_ITEM_COUNT 2
	double input[MAX_ITEM_COUNT];
	double output[MAX_ITEM_COUNT];

	//init input for xy2Sky
	int k=0;
	for(int i=0;i<MAX_ITEM_COUNT/2;++i, k+=2) {
		input[k] = i;
		input[k+1] = i;
	}

	if(init(fileName))
		xyToSky(input, output, MAX_ITEM_COUNT);
/*
	input[0] =  164.986878702;
	input[1] = -32.5906550412;
	if(init(fileName))
		skyToXY(input, output, MAX_ITEM_COUNT); */

	printf("\n FINAL x= %lf",output[0]);
	printf("\n FINAL y= %lf",output[1]);

	close();
}

//-----------------------------------------------------------------------------
//end of file "main.c"
//-----------------------------------------------------------------------------
