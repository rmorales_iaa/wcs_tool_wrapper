//Java Wrapper for wcstools.
//http://tdc-www.harvard.edu/wcstools/
//Adapted from wcstools. version 3.9.5
//-----------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <gnu/libc-version.h>
#include <time.h>
#include <locale.h>

#include "wcstools/libwcs/fitsfile.h"
#include "wcstools/libwcs/wcs.h"

//-----------------------------------------------------------------------------
//Global vars
//-----------------------------------------------------------------------------
struct WorldCoor * wcs =  NULL;

//-----------------------------------------------------------------------------
//External functions
//-----------------------------------------------------------------------------
extern struct WorldCoor *GetWCSFITS();	/* Read WCS from FITS or IRAF header */

extern int xy2sky (struct WorldCoor * wcs,
	double * input,
	double * output,
	unsigned long inputItemSize,
	int ac,
	char **av);

extern int sky2xy (struct WorldCoor * wcs,
    double * input,
	double * output,
	unsigned long inputItemSize,
	int ac,
	char **av);

//-----------------------------------------------------------------------------

void printStartInfo(char * fn){

    time_t rawtime;
	struct tm * timeinfo;
	char buffer [80];
	time ( &rawtime );
	timeinfo = localtime ( &rawtime );


    setlocale (LC_ALL,"C");
	printf ("Wctools 3.9.5 java wrapper\n");
	printf ("\tWrapper Version 0.1\n");
	printf ("\tGNU libc version: %s\n", gnu_get_libc_version());
	printf ("\tLocale is: %s\n", setlocale(LC_ALL,NULL) );
	strftime (buffer,80,"%c",timeinfo);
	printf ("\tDate is: %s\n",buffer);
	printf ("\tLoading image '%s'\n",fn);
}

//-----------------------------------------------------------------------------

int init(char * fn) {

	 setlocale (LC_ALL,"C");
	//printStartInfo();
	if (isfits(fn) || isiraf(fn) || istiff(fn) || isjpeg(fn) || isgif(fn)) {

		wcs = GetWCSFITS (fn, 0);
		if (nowcs (wcs)) {
			printf ("%s: No WCS for file, cannot compute image size\n", fn);
			return -1;
		}
		return -1;
	}
	printf ("No input file or file not valid\n");
	return 0;
}

//-----------------------------------------------------------------------------
void close(void) {

	if (wcs != NULL) {
		wcsfree (wcs);
		wcs = NULL;
	}
}

//-----------------------------------------------------------------------------
int xyToSky(double * input,
	double * output,
	unsigned long inputItemSize) {

	char * parameterList [] = {"fake_program_name", "-jd", "-n", "10", "fake_image_name"};

	return xy2sky(wcs, input, output, inputItemSize, 5, parameterList);
}

//-----------------------------------------------------------------------------
int skyToXY(double * input,
	double * output,
	unsigned long inputItemSize) {

	char * parameterList [] = {"fake_program_name", "-j", "-n", "10", "fake_image_name"};
	return sky2xy(wcs, input, output, inputItemSize, 5, parameterList);

}

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//end of file "main.c"
//-----------------------------------------------------------------------------
