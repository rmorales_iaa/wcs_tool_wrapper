//Adapted from wcstools. version 3.9.5
//http://tdc-www.harvard.edu/wcstools/

/* File xy2sky.c
 * September 22, 2010
 * By Jessica Mink, Harvard-Smithsonian Center for Astrophysics
 * Send bug reports to jmink@cfa.harvard.edu

   Copyright (C) 1996-2010
   Smithsonian Astrophysical Observatory, Cambridge, MA USA

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <math.h>
#include "libwcs/wcs.h"
#include "libwcs/fitsfile.h"
#include "libwcs/wcscat.h"

static void PrintUsage();
extern struct WorldCoor *GetWCSFITS();	/* Read WCS from FITS or IRAF header */
extern void setsys(),setcenter(),setsecpix(),setnpix(),setrefpix(),setdateobs();
static void PrintHead();

static int verbose = 0;		/* verbose/debugging flag */
static int append = 0;		/* append input line flag */
static int tabtable = 0;	/* tab table output flag */
static int identifier = 0;	/* 1st column=id flag */
static char coorsys[16];
static int linmode = -1;
static int face = 1;
static int ncm = 0;
static int printhead = 0;
static char printonly = 'n';
static int version = 0;		/* If 1, print only program name and version */

static char *RevMsg = "XY2SKY WCSTools 3.9.5, 30 March 2017, Jessica Mink (jmink@cfa.harvard.edu)";

//-----------------------------------------------------------------------------

int
configure_xy2sky(wcs, ac, av)
struct WorldCoor * wcs;
int ac;
char **av;
{
	char *str, *str1;
	char wcstring[64];
	char lstr = 64;
	int ndecset = 0;
	int degout = 0;
	int ndec = 3;
	int nlines, iline;
	int entx = 0;
	int enty = 0;
	int entmag = 0;
	int i, ic;
	double x, y, mag;
	double cra, cdec, dra, ddec, secpix;
	double eqout = 0.0;
	int sysout = 0;
	int wp, hp, nx, ny, lhead;
	FILE *fd = NULL;
	char *ln, *listname;
	char linebuff[1024];
	char *line;
	char *header;
	char *fn;
	int bitpix = 0;
	char xstr[32], ystr[32], mstr[32];
	char rastr[32], decstr[32];
	char keyword[16];
	char temp[64];
	int ncx = 0;
	char *cofile;
	char *cobuff;
	char *space, *cstr, *dstr;
	int nterms = 0;
	double mag0, magx;
	double coeff[5];
	struct Tokens tokens;
	struct TabTable *tabxy;
	struct WorldCoor *GetFITSWCS();

	*coorsys = 0;
	cofile = NULL;


	/* Check for help or version command first */
	str = *(av+1);
	if (!str || !strcmp (str, "help") || !strcmp (str, "-help"))
		PrintUsage (str);
	if (!strcmp (str, "version") || !strcmp (str, "-version")) {
		version = 1;
		PrintUsage (str);
	}

	/* crack arguments */
	for (av++; --ac > 0 && *(str = *av) == '-'; av++) {
		char c;
		while ((c = *++str))
			switch (c) {

			case 'v':	/* more verbosity */
				verbose++;
				break;

			case 'a':	/* Append input line to sky position */
				append++;
				break;

			case 'b':	/* initial coordinates on command line in B1950 */
				str1 = *(av+1);
				ic = (int)str1[0];
				if (*(str+1) || ic < 48 || ic > 58) {
					setsys(WCS_B1950);
					sysout = WCS_B1950;
					eqout = 1950.0;
				}
				else if (ac < 3)
					PrintUsage (str);
				else {
					setsys (WCS_B1950);
					sysout = WCS_B1950;
					eqout = 1950.0;
					strcpy (rastr, *++av);
					ac--;
					strcpy (decstr, *++av);
					ac--;
					setcenter (rastr, decstr);
				}
				strcpy (coorsys,"B1950");
				break;


			case 'c':	/* Magnitude conversion coefficients */
				if (ac < 2)
					PrintUsage (str);
				cofile = *++av;
				ac--;
				break;

			case 'd':	/* Output degrees instead of hh:mm:ss dd:mm:ss */
				degout++;
				break;

			case 'e':	/* Output galactic coordinates */
				str1 = *(av+1);
				ic = (int)str1[0];
				if (*(str+1) || ic < 48 || ic > 58) {
					setsys (WCS_ECLIPTIC);
					sysout = WCS_ECLIPTIC;
					eqout = 2000.0;
				}
				else if (ac < 3)
					PrintUsage (str);
				else {
					setsys (WCS_ECLIPTIC);
					sysout = WCS_ECLIPTIC;
					eqout = 2000.0;
					strcpy (rastr, *++av);
					ac--;
					strcpy (decstr, *++av);
					ac--;
					setcenter (rastr, decstr);
				}
				strcpy (coorsys,"ecliptic");
				degout++;
				break;

			case 'f':	/* Face to use for projections with 3rd dimension */
				if (ac < 2)
					PrintUsage (str);
				face = atoi (*++av);
				(void) wcszin (face);
				ac--;
				break;

			case 'g':	/* Output galactic coordinates */
				str1 = *(av+1);
				ic = (int)str1[0];
				if (*(str+1) || ic < 48 || ic > 58) {
					setsys (WCS_GALACTIC);
					sysout = WCS_GALACTIC;
					eqout = 2000.0;
				}
				else if (ac < 3)
					PrintUsage (str);
				else {
					setsys (WCS_GALACTIC);
					sysout = WCS_GALACTIC;
					eqout = 2000.0;
					strcpy (rastr, *++av);
					ac--;
					strcpy (decstr, *++av);
					ac--;
					setcenter (rastr, decstr);
				}
				strcpy (coorsys,"galactic");
				degout++;
				break;

			case 'h':	/* Print column heading */
				printhead++;
				break;

			case 'i':	/* 1st column = star id */
				identifier++;
				break;

			case 'j':       /* center coordinates on command line in J2000 */
				str1 = *(av+1);
				ic = (int)str1[0];
				if (*(str+1) || ic < 48 || ic > 58) {
					setsys(WCS_J2000);
					sysout = WCS_J2000;
					eqout = 2000.0;
				}
				else if (ac < 3)
					PrintUsage (str);
				else {
					setsys(WCS_J2000);
					sysout = WCS_J2000;
					eqout = 2000.0;
					strcpy (rastr, *++av);
					ac--;
					strcpy (decstr, *++av);
					ac--;
					setcenter (rastr, decstr);
				}
				strcpy (coorsys,"J2000");
				break;

			case 'k':	/* column for X; Y is next column */
				ncx = atoi (*++av);
				ac--;
				break;

			case 'l':	/* Mode for output of linear coordinates */
				if (ac < 2)
					PrintUsage (str);
				linmode = atoi (*++av);
				ac--;
				break;

			case 'm':	/* column for magnitude */
				ncm = atoi (*++av);
				ac--;
				break;

			case 'n':	/* Number of decimal places in output sec or deg */
				if (ac < 2) PrintUsage (str);
				ndec = atoi (*++av);
				ndecset++;
				ac--;
				break;

			case 'o':   /* Output only the following part of the coordinates */
				if (ac < 2) PrintUsage (str);
				av++;
				printonly = **av;
				ac--;
				break;

			case 'p':       /* Initial plate scale in arcseconds per pixel */
				if (ac < 2) PrintUsage (str);
				setsecpix (atof (*++av));
				ac--;
				break;

			case 'q':	/* Equinox for output */
				if (ac < 2) PrintUsage (str);
				strcpy (coorsys, *++av);
				ac--;
				break;

			case 's':   /* Size of image in X and Y pixels */
				if (ac < 3) PrintUsage(str);
				nx = atoi (*++av);
				ac--;
				ny = atoi (*++av);
				ac--;
				setnpix (nx, ny);
				break;

			case 't':	/* Output tab table */
				tabtable++;
				break;

			case 'x':       /* X and Y coordinates of reference pixel */
				if (ac < 3) PrintUsage (str);
				x = atof (*++av);
				ac--;
				y = atof (*++av);
				ac--;
				setrefpix (x, y);
				break;

			case 'y':       /* Epoch of image in FITS date format */
				if (ac < 2) PrintUsage (str);
				setdateobs (*++av);
				ac--;
				break;

			case 'z':	/* Use AIPS classic WCS */
				setdefwcs (WCS_ALT);
				break;

			default:
				PrintUsage (str);
				break;
			}
	}

	if (linmode > -1)
		setwcslin (wcs, linmode);
	if (wcs->sysout == WCS_B1950)
		wcsoutinit (wcs, "B1950");
	if (wcs->sysout == WCS_J2000)
		wcsoutinit (wcs, "J2000");
	if (*coorsys)
		wcsoutinit (wcs, coorsys);

	/* Get magnitude conversion polynomial coefficients */
	if (cofile != NULL) {
		cobuff = getfilebuff (cofile);
		mag0 = 0.0;
		(void) agetr8 (cobuff, "mag0", &mag0);
		for (i = 0; i < 5; i++) {
			coeff[i] = 0.0;
			sprintf (keyword, "mcoeff%d", i);
			(void) agetr8 (cobuff, keyword, &coeff[i]);
			if (coeff[i] != 0.0) nterms = i + 1;
		}
		if (ncm == 0)
			ncm = 3;
	}
	if (degout) {
		wcs->degout = degout;
		if (!ndecset) {
			ndec = 5;
			ndecset++;
		}
	}
	if (tabtable)
		wcs->tabsys = 1;
	if (ndecset)
		wcs->ndec = ndec;


	else if (isnum (*av)) {
		fn = NULL;
		lhead = 14400;
		header = (char *) calloc (1, lhead);
		strcpy (header, "END ");
		for (i = 4; i < lhead; i++)
			header[i] = ' ';
		hlength (header, 14400);
		hputl (header, "SIMPLE", 1);
		hputi4 (header, "BITPIX", bitpix);
		hputi4 (header, "NAXIS", 2);
		hputi4 (header, "NAXIS1", 100);
		hputi4 (header, "NAXIS2", 100);
		wcs = GetFITSWCS (fn,header,verbose,&cra,&cdec,&dra,&ddec,&secpix,
				&wp, &hp, &sysout, &eqout);
		if (nowcs (wcs)) {
			fprintf (stderr, "Incomplete WCS on command line\n");
			free (header);
			return -1;
		}
	}

	return 1;
}

//-----------------------------------------------------------------------------
static void
PrintUsage (command)
char	*command;

{
}

//-----------------------------------------------------------------------------
static void
PrintHead (fn, wcs, tabxy, listfile)

char	*fn;		/* Name of file containing list of x,y coordinates */
struct WorldCoor *wcs;	/* World coordinate system structure */
struct TabTable *tabxy;	/* tab table structure */
char *listfile;		/* Name of file with list of input coordinates */

{
}
//-----------------------------------------------------------------------------

int xy2sky (struct WorldCoor * wcs,
    double * input,
	double * output,
	unsigned long inputItemSize,
	int ac,
	char **av){

	if (!configure_xy2sky(wcs, ac, av)){
		return -1;
	}
	else {

		unsigned long i = 0;
		unsigned long k = 0;
		unsigned long max = inputItemSize / 2;
		for(i =0; i<max; ++i){

			pix2wcs (wcs, input[k], input[k+1], &output[k], &output[k+1]);
			k+=2;
		}
	}
	return 1;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
