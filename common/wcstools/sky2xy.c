//Adapted from wcstools. version 3.9.5
//http://tdc-www.harvard.edu/wcstools/

/* File sky2xy.c
 * July 22, 2015
 * By Jessica Mink, Harvard-Smithsonian Center for Astrophysics
 * Send bug reports to jmink@cfa.harvard.edu

   Copyright (C) 1996-2015
   Smithsonian Astrophysical Observatory, Cambridge, MA USA

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <math.h>
#include "libwcs/fitswcs.h"

static void PrintUsage();
extern void setrot(),setsys(),setcenter(),setsecpix(),setrefpix(),setdateobs();
extern void setnpix();
extern struct WorldCoor *GetFITSWCS ();	/* Read WCS from FITS or IRAF header */
static int version = 0;		/* If 1, print only program name and version */

static char *RevMsg = "SKY2XY WCSTools 3.9.5, 30 March 2017, Jessica Mink (jmink@cfa.harvard.edu)";
char csys[16];

int
configure_sky2XY(wcs, ac, av)
struct WorldCoor * wcs;
int ac;
char **av;
{
	int verbose = 0; /* verbose/debugging flag */
	char *str;
	double x, y, ra, dec, ra0, dec0;
	FILE *fd;
	char *ln, *listname;
	char line[80];
	char *fn;
	int sysin;
	char rastr[32], decstr[32];
	char xstr[16], ystr[16];
	char *str1;
	int ic;
	int offscale, n;
	int nx, ny, lhead, i, nf;
	int bitpix = 0;
	char *header;
	double cra, cdec, dra, ddec, secpix, drot;
	double eqout = 0.0;
	double eqin = 0.0;
	int sysout = 0;
	int syscoor = 0;
	int degout = 0;
	int wp, hp;
	char coorsys[16];
	int ndec = 3; /* Number of decimal places in output coords */
	char printonly = 'b';
	int modwcs = 0; /* 1 if image WCS modified on command line */

	/* Check for help or version command first */
	str = *(av + 1);
	if (!str || !strcmp(str, "help") || !strcmp(str, "-help"))
		PrintUsage(str);
	if (!strcmp(str, "version") || !strcmp(str, "-version")) {
		version = 1;
		PrintUsage(str);
	}

	*coorsys = 0;

	/* Decode arguments */
	for (av++; --ac > 0 && *(str = *av) == '-'; av++) {
		char c;
		while ((c = *++str))
			switch (c) {

			case 'v': /* more verbosity */
				verbose++;
				break;

			case 'a': /* Initial rotation angle in degrees */
				if (ac < 2)
					PrintUsage(str);
				drot = atof(*++av);
				setrot(drot);
				modwcs = 1;
				ac--;
				break;

			case 'b': /* image reference coordinates on command line in B1950 */
				str1 = *(av + 1);
				ic = (int) str1[0];
				if (*(str + 1) || ic < 48 || ic > 58) {
					strcpy(coorsys, "B1950");
				} else if (ac < 3)
					PrintUsage(str);
				else {
					setsys(WCS_B1950);
					sysout = WCS_B1950;
					syscoor = WCS_B1950;
					eqout = 1950.0;
					if (strlen(*++av) < 32)
						strcpy(rastr, *av);
					else {
						strncpy(rastr, *av, 31);
						rastr[31] = (char) 0;
					}
					ac--;
					if (strlen(*++av) < 32)
						strcpy(decstr, *av);
					else {
						strncpy(decstr, *av, 31);
						decstr[31] = (char) 0;
					}
					ac--;
					setcenter(rastr, decstr);
					modwcs = 1;
				}
				break;

			case 'e':
				str1 = *(av + 1);
				ic = (int) str1[0];
				if (*(str + 1) || ic < 48 || ic > 58) {
					setsys(WCS_ECLIPTIC);
					sysout = WCS_ECLIPTIC;
					syscoor = WCS_ECLIPTIC;
					eqout = 2000.0;
				} else if (ac < 3)
					PrintUsage(str);
				else {
					setsys(WCS_ECLIPTIC);
					sysout = WCS_ECLIPTIC;
					syscoor = WCS_ECLIPTIC;
					eqout = 2000.0;
					strcpy(rastr, *++av);
					ac--;
					strcpy(decstr, *++av);
					ac--;
					setcenter(rastr, decstr);
					modwcs = 1;
				}
				degout++;
				strcpy(coorsys, "ecliptic");
				break;

			case 'g':
				str1 = *(av + 1);
				ic = (int) str1[0];
				if (*(str + 1) || ic < 48 || ic > 58) {
					setsys(WCS_GALACTIC);
					sysout = WCS_GALACTIC;
					syscoor = WCS_GALACTIC;
					eqout = 2000.0;
				} else if (ac < 3)
					PrintUsage(str);
				else {
					setsys(WCS_GALACTIC);
					sysout = WCS_GALACTIC;
					syscoor = WCS_GALACTIC;
					eqout = 2000.0;
					strcpy(rastr, *++av);
					ac--;
					strcpy(decstr, *++av);
					ac--;
					setcenter(rastr, decstr);
					modwcs = 1;
				}
				degout++;
				strcpy(coorsys, "galactic");
				break;

			case 'j': /* image reference coordinates on command line in J2000 */
				str1 = *(av + 1);
				ic = (int) str1[0];
				if (*(str + 1) || ic < 48 || ic > 58) {
					strcpy(coorsys, "J2000");
				} else if (ac < 3)
					PrintUsage(str);
				else {
					setsys(WCS_J2000);
					sysout = WCS_J2000;
					syscoor = WCS_J2000;
					eqout = 2000.0;
					if (strlen(*++av) < 32)
						strcpy(rastr, *av);
					else
						strncpy(rastr, *av, 31);
					rastr[31] = (char) 0;
					ac--;
					if (strlen(*++av) < 32)
						strcpy(decstr, *av);
					else
						strncpy(decstr, *av, 31);
					decstr[31] = (char) 0;
					ac--;
					setcenter(rastr, decstr);
					modwcs = 1;
				}
				break;

			case 'n': /* Number of decimal places in output coordsinates */
				if (ac < 2)
					PrintUsage(str);
				ndec = (int) atof(*++av);
				ac--;
				break;

			case 'o': /* Output only the following part of the coordinates */
				if (ac < 2)
					PrintUsage(str);
				av++;
				printonly = **av;
				ac--;
				break;

			case 'p': /* Initial plate scale in arcseconds per pixel */
				if (ac < 2)
					PrintUsage(str);
				setsecpix(atof(*++av));
				modwcs = 1;
				ac--;
				break;

			case 's': /* Size of image in X and Y pixels */
				if (ac < 3)
					PrintUsage(str);
				nx = atoi(*++av);
				ac--;
				ny = atoi(*++av);
				ac--;
				setnpix(nx, ny);
				modwcs = 1;
				break;

			case 'x': /* X and Y coordinates of reference pixel */
				if (ac < 3)
					PrintUsage(str);
				x = atof(*++av);
				ac--;
				y = atof(*++av);
				ac--;
				setrefpix(x, y);
				modwcs = 1;
				break;

			case 'y': /* Epoch of image in FITS date format */
				if (ac < 2)
					PrintUsage(str);
				setdateobs(*++av);
				ac--;
				modwcs = 1;
				break;

			case 'z': /* Use AIPS classic WCS */
				setdefwcs(WCS_ALT);
				break;

			default:
				PrintUsage(str);
				break;
			}
	}

	/* There are ac remaining file names starting at av[0] */
	if (ac == 0)
		PrintUsage(str);

	/* Set size of image coordinate field */
	if (wcs->nxpix < 10 && wcs->nypix < 10)
		nf = 2 + ndec;
	else if (wcs->nxpix < 100 && wcs->nypix < 100)
		nf = 3 + ndec;
	else if (wcs->nxpix < 1000 && wcs->nypix < 1000)
		nf = 4 + ndec;
	else if (wcs->nxpix < 10000 && wcs->nypix < 10000)
		nf = 5 + ndec;
	else
		nf = 6 + ndec;

	if (degout)
		wcs->degout = degout;

	 if (*coorsys)
		strcpy(csys, coorsys);
	 else {
		 if (wcs->prjcode < 0)
			strcpy(csys, "PIXEL");
		 else
			 if (wcs->prjcode < 2)
				 strcpy(csys, "LINEAR");
			 else
				 strcpy(csys, wcs->radecsys);
	 }

	 sysin = wcscsys(csys);
	 eqin = wcsceq(csys);
	 if (wcs->syswcs > 0 && wcs->syswcs != 6 && wcs->syswcs != 10)
		 wcscon(sysin, wcs->syswcs, eqin, eqout, &ra, &dec, wcs->epoch);


	return 1;
}

//-----------------------------------------------------------------------------
static void
PrintUsage (command)
char	*command;
{
}

//-----------------------------------------------------------------------------
int sky2xy (struct WorldCoor * wcs,
    double * input,
	double * output,
	unsigned long inputItemSize,
	int ac,
	char **av){

	if (!configure_sky2XY(wcs, ac, av)){
		return -1;
	}
	else {
		unsigned long i = 0;
		unsigned long k = 0;
		unsigned long max = inputItemSize / 2;
		int offscale;
		for(i =0; i<max; ++i){

			wcsc2pix(wcs, input[k], input[k+1], csys,  &output[k], &output[k+1], &offscale);
			k+=2;
		}
	}
	return 1;
}

//-----------------------------------------------------------------------------
